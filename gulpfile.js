'use strict';

var gulp = require('gulp'),
		pug = require('gulp-pug'),
		htmlbeautify = require('gulp-html-beautify'),
    sourcemaps = require('gulp-sourcemaps'),
		uglify = require( 'gulp-uglify' ),
    autoprefixer = require('gulp-autoprefixer'),
    browserSync = require("browser-sync"),
		sass = require( 'gulp-sass' ),
		imagemin = require( 'gulp-imagemin' ),
    pngquant = require( 'imagemin-pngquant' ),
		rimraf = require('rimraf'),
    reload = browserSync.reload,
		mainBowerFiles = require('gulp-main-bower-files');


var path = {
  build: {
      html: 'build/',
      css: 'build/css/',
      js: 'build/js/',
      img: 'build/img/',
      fonts: 'build/fonts/'
  },
  src: {
      views: 'src/views/*.pug',
      js: 'src/js/*.js',
      sass: 'src/sass/*.scss',
      img: 'src/img/**/*.*',
      fonts: 'src/fonts/**/*.*'
  },
  watch: {
      views: 'src/views/*.pug',
      js: 'src/js/**/*.js',
      sass: 'src/sass/**/*.scss',
      img: 'src/img/**/*.*',
      fonts: 'src/fonts/**/*.*'
  },
  clean: './build'
};

var config = {
  server: {
      baseDir: "build/"
  },
  tunnel: false,
  host: 'localhost',
  port: 3000,
};

gulp.task('serve', function () {
  browserSync(config);
});

gulp.task('bower-files', function buildBower() {
    return gulp.src('bower.json')
        .pipe(mainBowerFiles({
            overrides: {
                jquery: {
                    main: ['./dist/jquery.min.js']
                }
            }
        }))
        .pipe(gulp.dest('build/lib'));
});

gulp.task('pug:build', function buildHTML() {
  var options = {indentSize: 2};
  gulp.src(path.src.views)
	  .pipe(pug())
    .pipe(htmlbeautify(options))
	  .pipe(gulp.dest(path.build.html))
		.pipe(reload({stream: true}));
});

gulp.task('js:build', function buildJS() {
  gulp.src(path.src.js)
    .pipe(uglify())
    .pipe(gulp.dest(path.build.js))
    .pipe(reload({stream: true}));
});

gulp.task('sass:build', function buildSaaS() {
  gulp.src(path.src.sass)
		.pipe(sourcemaps.init())
		.pipe(sass({
        includePaths: ['src/sass/'],
        // outputStyle: 'compressed',
				sourceMap: true,
        errLogToConsole: true
    }).on( 'error', sass.logError ))
		.pipe(autoprefixer())
		.pipe(sourcemaps.write())
    .pipe(gulp.dest(path.build.css))
    .pipe(reload({stream: true}));
});

gulp.task('image:build', function buildImage() {
  gulp.src(path.src.img)
    .pipe(imagemin({
        progressive: true,
        svgoPlugins: [{removeViewBox: false}],
        use: [pngquant()],
        interlaced: true
    }))
    .pipe(gulp.dest(path.build.img))
    .pipe(reload({stream: true}));
});

gulp.task('copy', function() {
  gulp.src(path.src.fonts)
      .pipe(gulp.dest(path.build.fonts));
});

gulp.task('clean', function clean(cb) {
    rimraf(path.clean, cb);
});

gulp.task('build', [
  'pug:build',
	'sass:build',
  'js:build',
  'image:build'
]);

gulp.task('watch', function watch(){
	gulp.watch(path.watch.views, ['pug:build']);
	gulp.watch(path.watch.sass, ['sass:build']);
	gulp.watch(path.watch.js, ['js:build']);
	gulp.watch(path.watch.img, ['image:build']);
});

gulp.task('dev', ['build', 'copy', 'bower-files', 'serve', 'watch']);
