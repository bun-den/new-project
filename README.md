# klepteam

## Pre-Requisites

- Node.js installed (v6.0.0 or higher)
- `npm` installed (installed with Node, but needs to be updated with `npm update -g`)
- `bower` installed (`npm install -g bower`)
- Run `npm install && bower install` while CD'ed into the root level of the project

#### Atom

- **Linter-ESLint** - <https://atom.io/packages/linter-eslint>

  - **Linter** (dependency) - <https://atom.io/packages/linter>

- **Linter-HTMLHint** - <https://atom.io/packages/linter-htmlhint>

  - **Linter** (dependency) - <https://atom.io/packages/linter>

- **Linter-StyleLint** - <https://atom.io/packages/linter-stylelint>

  - **Linter** (dependency) - <https://atom.io/packages/linter>

- **PUG** - <https://atom.io/packages/language-pug>

## Update Dependencies

```
npm update
npm prune

bower update
bower prune
```
## Developing

```
npm run dev
```
