jQuery(document).ready(function($){

	//--------------------- navigation -----------------------//

	var contentSections = $('.cd-section'),
		navigationItems = $('#cd-vertical-nav a'),
    contentBlocks = $('.cd-block'),
    imageItems = $('.cd-images-nav img');

	updateNavigation();
	updateImages();
	$(window).on('scroll', function(){
		updateNavigation();
		updateImages();
	});

	//smooth scroll to the section
	navigationItems.on('click', function(event){
        event.preventDefault();
        smoothScroll($(this.hash));
    });
    //smooth scroll to second section
    // $('.cd-scroll-down').on('click', function(event){
    //     event.preventDefault();
    //     smoothScroll($(this.hash));
    // });

	$('.cd-nav-trigger').on('click', function(){
        	$('#cd-vertical-nav').toggleClass('open');
        });
        $('#cd-vertical-nav a').on('click', function(){
        	$('#cd-vertical-nav').removeClass('open');
        });

    var stickyNav = function() {
      if (!checkWindowWidth()) {

        // var stickyNavTop = $('#cd-vertical-nav').offset().top;
        // var scrollTop = $(window).scrollTop();
            // if (scrollTop > stickyNavTop) {
            //   $('#cd-vertical-nav').css('position','fixed');
            // } else {
            //   $('#cd-vertical-nav').css('position','absolute');
            // }
      } else if (checkWindowWidth()) {
      }
    };
    stickyNav();
    // $(window).scroll(function() {
    //     stickyNav();
    // });
    $(window).bind('resize orientationchange', function() {
      stickyNav();
    });

    //open-close navigation on touch devices
    // $('.touch .cd-nav-trigger').on('click', function(){
    // 	$('.touch #cd-vertical-nav').toggleClass('open');
    //
    // });

    //close navigation on touch devices when selectin an elemnt from the list
    // $('.touch #cd-vertical-nav a').on('click', function(){
    // 	$('.touch #cd-vertical-nav').removeClass('open');
    // });

	function updateNavigation() {
		contentSections.each(function(){
			$this = $(this);
			var activeSection = $('#cd-vertical-nav a[href="#'+$this.attr('id')+'"]').data('number') - 1;
			if ( ( $this.offset().top - $(window).height()/2 < $(window).scrollTop() ) && ( $this.offset().top + $this.height() - $(window).height()/2 > $(window).scrollTop() ) ) {
				navigationItems.eq(activeSection).addClass('is-selected');
			}else {
				navigationItems.eq(activeSection).removeClass('is-selected');
			}
		});
	}

	function updateImages() {
		contentBlocks.each(function(){
			$this = $(this);

			var index = $this.data('img');
			var section = $this.data('section');
			var activeImg = $('#section' + section + 'img' + index);

			if ( ( $this.offset().top - $(window).height()/2 < $(window).scrollTop() ) && ( $this.offset().top + $this.height() - $(window).height()/2 > $(window).scrollTop() ) ) {
				activeImg.addClass('img-visible');
			} else if ( index == 1 && ($this.offset().top - $(window).height()/2 > $(window).scrollTop())) {
				activeImg.addClass('img-visible');
			} else if ( (index == (getImageItems(section).length)) && ($this.offset().top + $this.height() - $(window).height()/2 < $(window).scrollTop() )) {
				activeImg.addClass('img-visible');
			} else {
				activeImg.removeClass('img-visible');
			}
		});
	}

	function smoothScroll(target) {
        $('body,html').animate(
        	{'scrollTop':target.offset().top},
        	600
        );
	}

	function getImageItems(section) {
        var imageItems = $('#section' + section).find('.cd-images-nav img');

		return imageItems;
	}


	function checkWindowWidth() {
    //check window width (scrollbar included)
    var e = window,
      a = 'inner';
    if (!('innerWidth' in window)) {
      a = 'client';
      e = document.documentElement || document.body;
    }
    if (e[a + 'Width'] >= 768) {
      return true;
    } else {
      return false;
    }
  }


	//--------------------- _________ ------------------------//



});
